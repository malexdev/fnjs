// automatically expect() several different types of objects
const expect = require('chai').expect
const Promise = require('bluebird')

// define the handlers for each type
const handlers = {
  'boolean': (fn, expected) => {
    // console.log('Testing if %s(%s) === %s', fn.name, true, expected)
    expect(fn(true)).to.equal(expected)
    expect(fn(false)).to.equal(expected)
  },
  'number': (fn, expected) => {
    expect(fn(1)).to.equal(expected)
    expect(fn(0)).to.equal(expected)
    expect(fn(151)).to.equal(expected)
  },
  'string': (fn, expected) => {
    expect(fn('')).to.equal(expected)
    expect(fn('test')).to.equal(expected)
  },
  'object': (fn, expected) => {
    expect(fn({})).to.equal(expected)
    expect(fn({foo: 'bar'})).to.equal(expected)
    expect(fn({foo: function(){ return true }})).to.equal(expected)
    expect(fn({foo: ['bar']})).to.equal(expected)
    expect(fn({foo: {bar: 'baz'}})).to.equal(expected)
  },
  'plainobject': (fn, expected) => {
    expect(fn({})).to.equal(expected)
    expect(fn({foo: 'bar'})).to.equal(expected)
  },
  'plainobjectdeep': (fn, expected) => {
    expect(fn({})).to.equal(expected)
    expect(fn({foo: 'bar'})).to.equal(expected)
    expect(fn({foo: ['bar']})).to.equal(expected)
    expect(fn({foo: {bar: 'baz'}})).to.equal(expected)
  },
  'array': (fn, expected) => {
    expect(fn([])).to.equal(expected)
    expect(fn(['foo', 'bar'])).to.equal(expected)
    expect(fn([1, 2])).to.equal(expected)
  },
  'function': (fn, expected) => {
    expect(fn(() => { return true })).to.equal(expected)
    expect(fn(() => {})).to.equal(expected)
    expect(fn(function() {})).to.equal(expected)
  },
  'promise': (fn, expected) => {
    const promise = new Promise((resolve, reject) => setTimeout(resolve, 10))
    expect(fn(promise)).to.equal(expected)
  },
  'promisefn': (fn, expected) => {
    const pmfn = function() { return Promise.resolve(true) }
    expect(fn(pmfn)).to.equal(expected)
  },
  'callbackfn': (fn, expected) => {
    const cbfn = function(callback) { setTimeout(callback, 10) }
    expect(fn(cbfn)).to.equal(expected)
  },
  'null': (fn, expected) => {
    expect(fn(null)).to.equal(expected)
  },
  'undefined': (fn, expected) => {
    expect(fn(undefined)).to.equal(expected)
  },
}

// run fn with the various types of objects passed
function autoTyper(fn, types) {
  // types is an array of 'type': 'expected'. i.e. to check if bool passes and number doesn't, types would be {'boolean': true, 'number': false}
  Object.keys(types).forEach(type => {
    const expected = types[type]
    const handler = handlers[type]
    if (handler) {
      return it(`should return ${expected} when passed ${type}`, () => handler(fn, expected))
    } else {
      throw new Error(`Cannot autotype check with type '${type}': No handler for type.`)
    }
  })
}

// run fn with all handlers, and only the given handler should be true
function onlyTyper(fn, type) {
  const types = Object.keys(handlers).reduce((prev, current) => {
    prev[current] = current === type
    return prev
  }, {})
  return autoTyper(fn, types)
}

// run fn with all handlers, and all but the given handler should be true
function allButTyper(fn, type) {
  const types = Object.keys(handlers).reduce((prev, current) => {
    prev[current] = current !== type
    return prev
  }, {})
  return autoTyper(fn, types)
}

// run fn with all handlers, assuming false unless they are specified in the passed array.
// types in this case is an array of string identifiers for types to test.
function includingTyper(fn, types) {
  // convert types to an object.
  types = types.reduce((prev, current) => {
    prev[current] = true
    return prev
  }, {})
  
  // add all possible handlers
  const allTypes = Object.keys(handlers).reduce((prev, current) => {
    prev[current] = types[current] === undefined ? false : types[current]
    return prev
  }, {})
  
  // run tests
  return autoTyper(fn, allTypes)
}

module.exports = {
  run: autoTyper,
  only: onlyTyper,
  except: allButTyper,
  including: includingTyper
}