const expect = require('chai').expect
const type = require('../../lib/util/type')
const autotyper = require('../helpers/autotyper')

describe('type.js', () => {
  
  describe('isNumber()', () => autotyper.only(type.isNumber, 'number'))
  
  describe('isString()', () => autotyper.only(type.isString, 'string'))
  
  describe('isBool()', () => autotyper.only(type.isBool, 'boolean'))
  
  describe('isNull()', () => autotyper.only(type.isNull, 'null'))
  
  describe('isUndefined()', () => autotyper.only(type.isUndefined, 'undefined'))
  
  describe('isNullOrUndefined()', () => autotyper.including(type.isNullOrUndefined, ['null', 'undefined']))
  
  describe('isPrimitive()', () => autotyper.including(type.isPrimitive, ['boolean', 'string', 'number', 'null', 'undefined']))
  
  describe('isObject()', () => autotyper.including(type.isObject, ['object', 'plainobject', 'plainobjectdeep']))
  
  describe('isPlainObject()', () => autotyper.only(type.isPlainObject, 'plainobject'))
  
  describe('isPlainObjectDeep()', () => autotyper.including(type.isPlainObjectDeep, ['plainobject', 'plainobjectdeep']))
  
  describe('isArray()', () => autotyper.only(type.isArray, 'array'))
  
  describe('isFunction()', () => autotyper.including(type.isFunction, ['function', 'callbackfn', 'promisefn']))
  
  describe('isPromise()', () => autotyper.only(type.isPromise, 'promise'))
  
  describe('isCallbackFunction()', () => autotyper.only(type.isCallbackFunction, 'callbackfn'))
  
})