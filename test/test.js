const fs = require('fs')
const path = require('path')

// set up globals for the test
global.UNITTEST = true
global.APPROOT = path.join(__dirname, '..', 'lib')

// get all the test files in ./suites
const rxIsJsFile = /^.+\.js$/i
const files = fs.readdirSync(path.join(__dirname, 'suites'))
  .filter(file => rxIsJsFile.test(file))
  .map(file => path.join(__dirname, 'suites', file))
  
// set up initial test suite
describe('fnjs', () => {

  // require all the files
  files.forEach(filePath => require(filePath))
  
})



