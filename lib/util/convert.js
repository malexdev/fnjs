const Promise = require('bluebird')
const type = require('./type.js')

// take an object, figure out if it's an array or an object.
// convert the items in the array depending on what they are.
// should handle:
//  x being an actual array
//  x being an object (in which case it will iterate over the values)
//  x being an array of promises
//  x being an array of sync functions
//  x being an array of callback functions
//  x being a sync function (will iterate over the result if result is an object or array)
//  x being a promise function (will iterate over result if result is an object or array)
//  x being a callback function (will iterate over result if result is an object or array)
// will return a promise regardless- gotta have something concrete to go on.
function unknownArrayToValues(x) {
  if (type.isArray(x)) {
    
    // run through the array and convert each item if needed.
    const arrayAsPromised = x.map(item => {
      
    })
    
    return Promise.all()
    
    
  } else if (type.isPlainObject(x)) {
    
    // get the values, then iterate over those.
    const values = Object.keys(x).map(k => x[k])
    return Promise.resolve(values)
    
  } else if (type.isFunction(x)) {
    
    // 
    
  }
}

module.exports = {
  unknownArrayToValues
}