function isFunction(x) {
  return Object.prototype.toString.call(x) === '[object Function]';
}

// yeah, this is unfortunately a little clunky.
// the way it works is if you have a function that expects arguments, and might take a callback, you run this with the number of arguments.
// i.e. if you know `function foo(bar, baz){}` exists, you know it takes 2 arguments.
// if you want to see if it takes a callback too, you'd run `isCallbackFunction(foo, 2)`.
// if no args are passed (i.e. you run `isCallbackFunction(foo)`), assumes 0. 
function isCallbackFunction(x, args) {
  return isFunction(x) && x.length > (args || 0) // .length on a function returns the amount of arguments the function is set up to handle.
}

// functions that return promises look no different than sync functions.
// so bear in mind this doesn't check if a function returns a promise; it checks if a return value is a promise.
// i.e. `isPromise(Promise.resolve(true))` would return false; `isPromise(new Promise((resolve, reject) => {}))` would return true.
function isPromise(x) {
  return !isNullOrUndefined(x) && isFunction(x.then)
}

function isArray(x) {
  return Object.prototype.toString.call(x) === '[object Array]';
}

function isObject(x) {
  return Object.prototype.toString.call(x) === '[object Object]' && !isPromise(x)
}

function isPlainObject(x) {
  return Object.prototype.toString.call(x) === '[object Object]' && !isPromise(x) && objectValuesArePrimitive(x);
}

function isPlainObjectDeep(x) {
  return Object.prototype.toString.call(x) === '[object Object]' && !isPromise(x) && objectValuesArePrimitiveRecursive(x);
}

function isBool(x) {
  return typeof x === 'boolean'
}

function isString(x) {
  return typeof x === 'string'
}

function isNumber(x) {
  return typeof x === 'number'
}

function isNull(x) {
  return x === null
}

function isUndefined(x) {
  return x === undefined
}

function isNullOrUndefined(x) {
  return isNull(x) || isUndefined(x)
}

// returns true if the item is a bool/number/string
function isPrimitive(x) {
  return isNullOrUndefined(x) || isBool(x) || isString(x) || isNumber(x)
}

function objectValuesArePrimitive(x) {
  return Object.keys(x).map(key => x[key]).every(val => isPrimitive(val))
}

// returns true if either all object values are primitive or the value is an array or object (then recurses)
function objectValuesArePrimitiveRecursive(x) {
  return Object.keys(x).map(key => x[key]).every(val => {
    if (isPlainObject(val) || isArray(val)) {
      return objectValuesArePrimitiveRecursive(val)
    } else {
      return isPrimitive(val)
    }
  })
}

module.exports = {
  isFunction,
  isCallbackFunction,
  isPromise,
  isArray,
  isObject,
  isPlainObject,
  isPlainObjectDeep,
  isBool,
  isNumber,
  isString,
  isPrimitive,
  isNull,
  isUndefined,
  isNullOrUndefined
}