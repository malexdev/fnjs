/*

map should handle:
  
  arr being an actual array
  arr being an object (in which case it will iterate over the values)
  arr being an array of promises
  arr being an array of sync functions
  arr being an array of callback functions
  arr being a sync function (will iterate over the result if result is an object or array)
  arr being a promise function (will iterate over result if result is an object or array)
  arr being a callback function (will iterate over result if result is an object or array)
  
  iterator being a sync function
  iterator being a promise function
  iterator being a callback function

*/

const Promise = require('bluebird')

function map(arr, iterator) {
  
}

